const axios=require('axios')
var _=require('lodash')

  
axios.get('https://anapioficeandfire.com/api/books')
.then(response => Promise.all(response.data.map(x => axios.get(x.url))))
.then(responses =>  Promise.all(responses.map(response => Promise.all(response.data.povCharacters.map(axios.get)))))
.then(responses => _.orderBy(_.flatMap(responses),'data.name').forEach(x => console.log(x.data.name + (x.data.culture? (": " + x.data.culture) : ""))))





// axios.get('https://anapioficeandfire.com/api/books/1')
//   .then(response =>  Promise.all(response.data.povCharacters.map(axios.get)))
//   .then(responses => responses.sort((a, b) => a.data.name > b.data.name)
//                     .forEach(x => console.log(x.data.name + (x.data.culture? (": " + x.data.culture) : ""))))
//   .catch(console.log);

