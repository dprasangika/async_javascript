# An Introduction to Async JavaScript

## General

Slides:

https://docs.google.com/presentation/d/1yZFqtE8hYD8qQls7sL338BRFUolB0jXTBC8thEzv4ZY/edit?usp=sharing


Ported from Meetup group:

https://www.meetup.com/NodeWorkshops/


## Install Node

Node can be installed from here:

https://nodejs.org/en/

## Help?

Solutions are in the `solutions` folder.


## Exercises 

### Methods of debugging

This trace method can help you peer into the code to examine values without interrupting the flow.

```JavaScript
const trace = (tag, obj) => {
  console.log('--------------');
  console.log(tag + ': ', obj);
  console.log('--------------');
  return obj;
})
```

This can be used as follows:

```JavaScript
axios.get(usersUrl) 
  .then(response => resonse.data)
  .then(data => trace('Data from response', data))
  .then(data => data.users)
  .then(users => trace('Users', users)
  .then(users => console.log('User count is ' + users.length))
```

It is common to debug with `console.log` in JS, however VS Code lets you debug JavaScript using breakpoints - https://code.visualstudio.com/docs/editor/debugging.

There are also various other options to debug node code: https://nodejs.org/en/docs/guides/debugging-getting-started/

Chrome let's you run JS code in the terminal, you can also place breakpoints in there. To open this, right click and select inspect element.

> Daryn: Personally I only use `console.log` and unit tests. I am probably missing out on more advanced tools...

### Async\Await Functions Return Promises

A function that uses async/await will return a promise. 

```JavaScript
const getUsers = async () => {
  const users = await axios.get(usersUrl) 
  return users
}

getUsers()
  .then(u => console.log(u)) // same as .then(console.log) 
  .catch(e => console.error(e))
```

## Part 1

You can use callbacks, promises or aync\await to solve this problem.

### Exercise 1.1

#### Step 1

Use the forismatic API to fetch 1 phrase and print the phrase.

To see the solution with promises see: 03_phraseWithPromise.js

#### Step 2

Automatically tweet this phrase to the NoodleNode account.

Reuse the code found in `102_twitter.js`.


#### Step 3

Use the forismatic API to fetch 1 phrase 

Convert that phrase to Yoda speak

Tweet the resulting text

Reuse the code found in `103_yoda.js`


#### Spoiler

A solution to this can be found here: `06_challangeWithAsynAwait.js`

## Part 2

To complete these you need to install the axios library

- https://www.npmjs.com/package/axios
- https://github.com/axios/axios

To install this library, use the command line. Make sure you are at the same level at `package.json`. 

Then use:
```
npm install axios
```

That library will then get added to the list of dependencies in `package.json`. The next time you use `npm install` npm will install all of the listed dependencies.

We will be using Game of Thrones data from An Api of Ice And File - https://anapioficeandfire.com/

You don't need to register to use this API.

### Exercise 2.1

Display the name and culture for a single character, given a single character URL. 

### Exercise 2.2

Display a __list of URLs__ for all of the `povCharacters` in the __first book__. This is not the actual character data, just the URLs for all the `povCharacters`.

### Exercise 2.3

Display the name and culture for every `povCharacter` character that appears in the __first book__ of the series. (Only the first book).

#### Hints

It may be useful to use Array.map

- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map

It may be useful to use Promise.all 

- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/all


### Exercise 2.4

Display the name and culture for every `povCharacter` character that appears in the __entire book series__.

These should be ordered alphabetically by name, and then by culture. There should not be any duplicates. 

#### Hints

Lodash is an excellent (functional) library that could be useful for this exercise (sort, map etc).

- https://lodash.com/

#### Recommended Functions

These are some functions that may be useful to complete this exercise. They can be tested individually by writing code that uses one of these functions separately and then displays the results to the console. 

---

Write a function that will **fetch a single character**. 

- The input for this function should be a URL. 
- This function should return a promise that resolves to the actual character data. For example the data you get from here: https://anapioficeandfire.com/api/characters/583

Make sure to test the function that fetches a single user by calling it directly and displaying the results to the console.

---

Write a function that will **build a single character model** from the character data from the API. 

- The input for this function should be raw data returned from the API e.g. https://anapioficeandfire.com/api/characters/583. 
- This function should return an object with the `name` and `culture` of the character
- This function is synchronous when called as a standalone function

---

Write a function that will **fetch characters** from a list of URLs. 

- The input for this function should be an array of character URLs. 
- This function should return a promise that resolves to a list of character data (either all of the character data, or only the name and culture of the character). 
- This function should not return a list of promises
- This function should not return a single promise that resolves to a list of promises
- It should return a single promise that resolves to a list of character data.

Make sure to test this function by calling it directly and displaying the results to the console.

---

Write a function that will **fetch a single book**. 

- The input for this function should be a URL (or a book number). 
- This function should return a promise that resolves to the actual book data. For example the data you get from here: https://anapioficeandfire.com/api/books/1

Make sure to test this function by calling it directly and displaying the results to the console.

---

Write a function that will **get all PovCharacter URLs from book data**. 

- The input for this function should be the book data e.g. data from https://anapioficeandfire.com/api/books/1. 
- This function should return an array of URLs for the point of view characters
- This function is synchronous when called as a standalone function

---




